#FIRST TASK:

If I would build responsive design , I would like to start with mobile first approach.
And after that adapt design for tablets and desktops using media queries/sass , for example, it could look like this:
```
$breakpoint-large: 1024px;
$breakpoint-medium: 769px;
$breakpoint-small: 425px;
$max-width: 1280px;

@mixin media($minOrMaxWidth, $breakpoint) {
	@media screen and ($minOrMaxWidth: $breakpoint) {
		@content;
	}
}

And then in css file will write just like that :
 @include media(min-width, $breakpoint-medium) {
		Css content goes here ,for example:
		font-size: 24px;
}
```
But if we have existing design in desktop , then just do the same (but adapt it for mobile phones)

2) For mobile and desktop, images would be different. Because if somebody are using mobile phone only, it would be not good to load large images (which size would be bigger than mobile phone images).

Hopefully understood right this task.
