(() => {
	const createNode = (element) => {
		return document.createElement(element);
	}

	const appendNode = (parent, el) => {
		return parent.appendChild(el);
	}

	const youtubeApiKey = 'AIzaSyAwYTISCezN1h3Qw4z3StvJOpkwXbePBKI'
	const youtubeFetchUri = `https://www.googleapis.com/youtube/v3/search?part=snippet&q=pluralsight&type=video&key=${youtubeApiKey}`

	const videoList = document.getElementById('videoList');
	const videoListFragment = document.createDocumentFragment();

	fetch(youtubeFetchUri)
		.then(res => res.json())
		.then(responseJson => {
			const { items } = responseJson;
			if (items) {
				return (items || []).map(item => {
					const listItem = createNode('li');
					const img = createNode('img');
					img.className = "video-list__image";
					img.src = item.snippet.thumbnails.high.url;
					const paragraph = createNode('p');
					paragraph.className = "video-list__paragraph";
					paragraph.innerHTML = item.snippet.title;
					appendNode(listItem, img);
					appendNode(listItem, paragraph);
					appendNode(videoListFragment, listItem);
					appendNode(videoList, videoListFragment);
				})
			}
			else {
				console.log("Nothing/error/noData");
			}
		})
		.catch(error => {
			console.log(error);
		});
})();


// if the project grows bigger , then we can use nameSpace like : "const myNameSpace = myNameSpace || {}"
// and put properties and functions there in order not to pollute global scope.
